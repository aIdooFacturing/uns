package com.unomic.factory911.device.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceSpVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.mc_prgm.domain.McInfoVo;

@Service
@Repository
public class DeviceServiceImpl implements DeviceService{

	private final static String ADAPTER_SPACE= "com.factory911.adapter.";
	private final static String DEVICE_SPACE= "com.factory911.device.";
	
	private static final Logger logger = LoggerFactory.getLogger(DeviceServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;

	
	@Override
	@Transactional(value="txManager_ma")
	public String editLastDvcStatus(AdapterVo pureVo)
	{
		
		if(pureVo.getSpdLd() == null || pureVo.getSpdLd().equals(CommonCode.MSG_UNAVAIL)) {
			pureVo.setSpdLd(null);
		}
		
		if(pureVo.getFdOvrd() == null || pureVo.getFdOvrd().equals(CommonCode.MSG_UNAVAIL)) {
			pureVo.setFdOvrd(null);
		}
		
		if(pureVo.getChartStatus() == null || pureVo.getChartStatus().equals(CommonCode.MSG_UNAVAIL)) {
			pureVo.setChartStatus(null);
		}

		//didn't need. Calculate at server.
		//setVo.setPARAM_AVR_CYCLE_TIME((pureVo.getAvrCycleTime().equals(CommonCode.MSG_UNAVAIL))?null:pureVo.getAvrCycleTime());
		
//		if( pureVo.getChartStatus() != null && pureVo.getPrgmHead().length()>50){
//			pureVo.setPrgmHead(pureVo.getPrgmHead().substring(0, 50));
//		}else{
//			pureVo.setPrgmHead(pureVo.getPrgmHead());
//		}
		
		pureVo.setPrgmHead(setLimitStr(pureVo.getPrgmHead(),50));
		pureVo.setMainPrgmName(setLimitStr(pureVo.getMainPrgmName(),20));
		pureVo.setAlarmMsg1(setLimitStr(pureVo.getAlarmMsg1(),100));
		pureVo.setAlarmMsg2(setLimitStr(pureVo.getAlarmMsg2(),100));
		pureVo.setAlarmMsg3(setLimitStr(pureVo.getAlarmMsg3(),100));
		pureVo.setAlarmNum1(setLimitStr(pureVo.getAlarmNum1(),50));
		pureVo.setAlarmNum2(setLimitStr(pureVo.getAlarmNum2(),50));
		pureVo.setAlarmNum3(setLimitStr(pureVo.getAlarmNum3(),50));
		pureVo.setMdlM1(setLimitStr(pureVo.getMdlM1(),11));
		pureVo.setMdlM2(setLimitStr(pureVo.getMdlM2(),11));
		pureVo.setMdlM3(setLimitStr(pureVo.getMdlM3(),11));
		
		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntDvcLast", pureVo);
		if(cntDvc > 0){
			sql_ma.update(DEVICE_SPACE + "editDvcLast", pureVo);
		}else{
			sql_ma.insert(DEVICE_SPACE + "addDvcLast", pureVo);
		}
		
//		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntDvcLast", pureVo);
//		if(cntDvc >= 1){
//			//update
//			sql_ma.update(DEVICE_SPACE + "editDvcLast", pureVo);
//		}// else{
//			//insert
//		sql_ma.delete(DEVICE_SPACE + "rmDvcLast", pureVo);
//		sql_ma.insert(DEVICE_SPACE + "addDvcLast", pureVo);

		return "OK";
	}
	
	
	//pureVo 받아서 마지막 알람 코드, 메세지 가져오기
	//필요 없음.
	private DeviceVo getLastAlarm(AdapterVo inputVo){
		logger.info("inputVo:"+inputVo);
		DeviceVo rtnVo = new DeviceVo();
		
		List<String> listAlarmCode = new ArrayList<String>();
		List<String> listAlarmMsg = new ArrayList<String>();
		
		if(! inputVo.getAlarmNum1().equals(CommonCode.MSG_UNAVAIL)){
			listAlarmCode.add(inputVo.getAlarmNum1());
			listAlarmMsg.add(inputVo.getAlarmMsg1());
		}
		
		if(! inputVo.getAlarmNum2().equals(CommonCode.MSG_UNAVAIL)){
			listAlarmCode.add(inputVo.getAlarmNum2());
			listAlarmMsg.add(inputVo.getAlarmMsg2());
		}
		if(! inputVo.getAlarmNum3().equals(CommonCode.MSG_UNAVAIL)){
			listAlarmCode.add(inputVo.getAlarmNum3());
			listAlarmMsg.add(inputVo.getAlarmMsg3());
		}
		
		if(listAlarmCode.size() > 1){
			rtnVo.setLastAlarmCode(listAlarmCode.get(listAlarmCode.size()-1));
			rtnVo.setLastAlarmMsg(listAlarmMsg.get(listAlarmMsg.size()-1));
		}else{
			rtnVo.setLastAlarmCode(CommonCode.MSG_UNAVAIL);
			rtnVo.setLastAlarmMsg(CommonCode.MSG_UNAVAIL);
		}
		
		return rtnVo;
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String calcDeviceOptime(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		sql_ma.update(DEVICE_SPACE + "editCalcOptime",dvcVo);
		
		return "OK";
	}

	@Override
	@Transactional(value="txManager_ma")
	public String calcDvcOpPf(DeviceVo inputVo){
		
		int cnt = (int) sql_ma.selectOne(DEVICE_SPACE+"cntOpPf", inputVo);
		if(cnt>0){
			sql_ma.delete(DEVICE_SPACE+"rmvOpPf", inputVo);
		}
		sql_ma.insert(DEVICE_SPACE+"calcOpPf", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String calcDeviceTimes(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		
		//sql.update(DEVICE_SPACE + "editCalcOptime",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcIncycleTimeSP",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcWaitTime",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcAlarmTime",dvcVo);
		sql_ma.update(DEVICE_SPACE + "editCalcNoconnectionTime",dvcVo);
		
		//must update after editCalcIncycleTime
		sql_ma.update(DEVICE_SPACE + "editCalcCuttingTimeSP",dvcVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addDvcSummary(DeviceVo inputVo)
	{
		logger.info("@@@@@run addDvcSummary@@@@@");
		sql_ma.update(DEVICE_SPACE +"addDvcSummarySP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String calcDeviceTimesTest(DeviceVo dvcVo)
	{
		logger.info("@@@@@run calcDeviceOptime@@@@@");
		sql_ma.update(DEVICE_SPACE +"editCalcIncycleTimeSP",dvcVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addTimeChart(DeviceVo inputVo)
	{
		sql_ma.insert(DEVICE_SPACE +"addTimeChartSP",inputVo);
		
		return "OK";
	}
	
	/*
	 * 작업 순서.
	 * 1. 장비와 마지막 통신 시간이 2분 이상이면서 상태가 no-connection이 아닌 장비 리스트 획득.
	 * 2. adt 마지막 상태들 endtime update 하고 no-connection 집어 넣음.
	 * 3. deviceLast no-connection으로 update.
	 *
	 * 추가될 내용.
	 * 1. tb_dvc_status 마지막 endTime update.
	 * 2. no-connection inset.
	 */
	
	@Override
	@Transactional(value="txManager_ma")
	public String editDvcLastNoCon_SP()
	{
		sql_ma.update(DEVICE_SPACE + "editDvcLastNoCon_SP");
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String adjustDeviceChartStatus_SP(DeviceStatusVo inputVo)
	{
		sql_ma.update(DEVICE_SPACE +"adjustDeviceChartStatus_SP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addDvcChartStatus(AdapterVo inputVo)
	{
		sql_ma.update(DEVICE_SPACE +"addDvcChartStatusSP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public AdapterVo getIdfromIp(AdapterVo inputVo)
	{
		AdapterVo rtnVo = (AdapterVo) sql_ma.selectOne(DEVICE_SPACE +"getIdfromIp", inputVo);
		
		return rtnVo;
	}
	
	
	@Override
	public List<McInfoVo> getListMcInfo()
	{
		List<McInfoVo> rtnList = (List<McInfoVo>) sql_ma.selectList(DEVICE_SPACE +"getListMcInfo");
		
		return rtnList;
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addDvcStatics(DeviceVo inputVo)
	{
		logger.error("@@@ Run addDvcStatics");
		 sql_ma.insert(DEVICE_SPACE +"addDvcStatics_SP", inputVo);
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String updateMcPrgmAvg()
	{
		 sql_ma.update(DEVICE_SPACE +"updateMcPrgmAvg");
		
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String mqt()
	{
		 String rtnStr = (String) sql_ma.selectOne(DEVICE_SPACE +"multiQueryTest");
		
		return rtnStr;
	}
	
	
	// 완
	@Override
	@Transactional(value="txManager_ma")
	public String editDvcLastTime(String sender)
	{
		String rtnStr = "FAIL";
		DeviceVo setVo = new DeviceVo();

		setVo.setDvcId(sender);
		
		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntDvcLast", setVo);
		if(cntDvc > 0){
			//update
			sql_ma.update(DEVICE_SPACE + "editDvcLastTime", setVo);
			rtnStr = "OK";
		}else{
			rtnStr = "UNKNOWN SENDER";
		}
		return rtnStr;
	}
	
	@Override
	public String addDvcDuple(String sender)
	{
		String rtnStr = "FAIL";
		DeviceVo setVo = new DeviceVo();

		setVo.setDvcId(sender);
		
		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntDvc", setVo);
		if(cntDvc > 0){
			//update
			sql_ma.insert(DEVICE_SPACE + "addDupleDvc", setVo);
			rtnStr = "OK";
		}else{
			rtnStr = "UNKNOWN SENDER";
		}
		return rtnStr;
	}
	
	String setLimitStr(String input,int limit){
		String rtnStr;
		if(input == null
				|| input.equals(CommonCode.MSG_UNAVAIL)
				|| input.equals("NaN")
			) {
			rtnStr = null;
		}else{
			int tmpLength = input.length();
			rtnStr = input.substring(0, (tmpLength > limit)?limit:tmpLength);
		}
		
		return rtnStr;
	}
	
	
	
}
