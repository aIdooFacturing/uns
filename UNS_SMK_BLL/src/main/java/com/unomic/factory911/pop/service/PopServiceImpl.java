package com.unomic.factory911.pop.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.pop.controller.PopController;
import com.unomic.factory911.pop.domain.McRepairVo;
import com.unomic.factory911.pop.domain.McStatVo;
import com.unomic.factory911.pop.domain.PopOtherJobVo;

@Service
@Repository
public class PopServiceImpl implements PopService{

	private final static String POP_SPACE= "com.factory911.pop.";
	private final static String DEVICE_SPACE= "com.factory911.device.";

	private static final Logger LOGGER = LoggerFactory.getLogger(PopController.class);
	
	@Autowired
	@Resource(name="sqlSession_ora")
	private SqlSession sql_ora;
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	@Override
	public List<McRepairVo> getRpView(McRepairVo inputVo)
	{
		//LOGGER.info("run getRpView");
		List<McRepairVo> rtnVo = (List<McRepairVo>) sql_ora.selectList(POP_SPACE+"getRpView", inputVo);
		return rtnVo;
	}
	
	public List<McStatVo> getMcCrt()
	{
		List<McStatVo> rtnVo = (List<McStatVo>) sql_ora.selectList(POP_SPACE+"getMcCrtStatus");
		return rtnVo;
	}
	
	public List<PopOtherJobVo> getOtherJobs()
	{
		List<PopOtherJobVo> rtnVo = (List<PopOtherJobVo>) sql_ora.selectList(POP_SPACE+"getMcOtherJobs");
		return rtnVo;
	}
	
	
	

	// 1. 대상 mc_no 가져오기.
	// 2. where mc_no 로 데이터 획득.
	// 3. insert to pop stack.
	// 4. delete and insert status table.
	
	@Override
	@Transactional(value="txManager_ma")
	public String syncPop()
	{
		List<DeviceVo> listRailRtn = sql_ma.selectList(DEVICE_SPACE +"getListRailMcNo");
		List<DeviceVo> listBlockRtn = sql_ma.selectList(DEVICE_SPACE +"getListBlockMcNo");
		
		HashMap <String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("listDvc", listRailRtn);
		List <McStatVo> listMcStat_rail = sql_ora.selectList(POP_SPACE + "getListPopDvc", dataMap);
		
		dataMap.put("listDvc", listBlockRtn);
		List <McStatVo> listMcStat_block = sql_ora.selectList(POP_SPACE + "getListPopDvc", dataMap);
		
 		dataMap.put("listPop",listMcStat_rail);
		sql_ma.insert(POP_SPACE+"addPopStack", dataMap);
		sql_ma.delete(POP_SPACE+"rmRailStatus");
		sql_ma.insert(POP_SPACE+"addRailStatus", dataMap);
		
		dataMap.put("listPop",listMcStat_block);
		sql_ma.insert(POP_SPACE+"addPopStack", dataMap);
		sql_ma.delete(POP_SPACE+"rmBlockStatus");
		sql_ma.insert(POP_SPACE+"addBlockStatus", dataMap);
		
		
		
		
		return "syncOK";
	}
	
	
	
	
	
}
