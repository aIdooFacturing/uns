package com.unomic.factory911.pop.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class McStatVo{

	String wonCd;
	String schGb;
	String mcNo;
	String jisiNo;
	String jobDate;
	String jobcd;
	String stdate;
	String fndate;
	String prSort2;
	String lmType;
	String mcNm;
	String updEmp;
	String updDate;
	String pgmId;

	Integer procNo;
	Integer railSize;
	Integer lmSize;
	Integer jobQty;
	Integer chksel;

}
