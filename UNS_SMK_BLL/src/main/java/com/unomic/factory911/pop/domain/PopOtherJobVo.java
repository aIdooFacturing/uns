package com.unomic.factory911.pop.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class PopOtherJobVo{
	String jobCd;
	String jobName;
	String mcNo;
	String mcName;
	String userName;
	String stDate;
	String fnDate;

	Integer jobMin;
	String repCause;
	String prSort2;
	Integer lmSize;
	Integer railSize;
	String lmType;
}
