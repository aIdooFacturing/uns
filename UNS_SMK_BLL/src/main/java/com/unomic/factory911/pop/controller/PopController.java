package com.unomic.factory911.pop.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.factory911.pop.domain.McStatVo;
import com.unomic.factory911.pop.domain.McRepairVo;
import com.unomic.factory911.pop.domain.PopOtherJobVo;
import com.unomic.factory911.pop.service.PopService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/pop")
@Controller
public class PopController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PopController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private PopService popService;
	

	@RequestMapping(value = "getListRep")
	@ResponseBody
	public List <McRepairVo> dbTest(HttpServletRequest request, McRepairVo inputVo){
		
		List <McRepairVo> listRep = (List<McRepairVo>) popService.getRpView(inputVo);
		
		return listRep;
	}
	
	@RequestMapping(value = "getOtherJobs")
	@ResponseBody
	public List <PopOtherJobVo> getOtherJobs(HttpServletRequest request){
		
		List <PopOtherJobVo> listRep = (List<PopOtherJobVo>) popService.getOtherJobs();
		
		return listRep;
	}
	
	@RequestMapping(value = "dbTest2")
	@ResponseBody
	public String dbTest2(HttpServletRequest request){
		
		List <McStatVo> listTestVo =  popService.getMcCrt();
		
		int size = listTestVo.size();
		LOGGER.info("size:"+size);
		for(int i=0;i<size;i++){
			LOGGER.info("index["+i+"]:"+listTestVo.get(i));
		}
		//LOGGER.info("testVo:" + testVo);
		return "OK";
	}
	@RequestMapping(value = "testSyncPop")
	@ResponseBody
	public String testSyncPop(HttpServletRequest request){
		
		popService.syncPop();
		return "OK";
	}

}
