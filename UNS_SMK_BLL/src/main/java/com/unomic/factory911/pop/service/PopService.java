package com.unomic.factory911.pop.service;

import java.util.List;

import com.unomic.factory911.pop.domain.McStatVo;
import com.unomic.factory911.pop.domain.McRepairVo;
import com.unomic.factory911.pop.domain.PopOtherJobVo;



public interface PopService {
	public List<McRepairVo> getRpView(McRepairVo inputVo);
	public List<McStatVo> getMcCrt();
	public String syncPop();
	public List<PopOtherJobVo> getOtherJobs();
}
