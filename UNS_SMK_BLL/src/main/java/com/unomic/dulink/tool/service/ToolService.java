package com.unomic.dulink.tool.service;

import java.util.ArrayList;

import com.unomic.dulink.tool.domain.ToolLifeVo;
import com.unomic.dulink.tool.domain.ToolOffsetVo;



public interface ToolService {
	public String addListToolLife(ArrayList<ToolLifeVo> listToolLife);
	public String addListToolOffset(ArrayList<ToolOffsetVo> listToolOffset);
}
