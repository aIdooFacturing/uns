package com.unomic.dulink.tool.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class ToolLifeVo{
	Integer dvcId;
	Integer grNo;
	Integer tlNo;
	Integer life;
	Integer cnt;

	String date;
	String crdt;
	String regdt;
}
