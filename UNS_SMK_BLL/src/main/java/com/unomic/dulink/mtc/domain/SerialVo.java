package com.unomic.dulink.mtc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class SerialVo{
	String mandt;
	String mcCd;
	String msrDt;
	String crtPgmBlc;
	String mod;
	String stt;
	String crtPgmNm;
	String spdLd;
	String actFd;
	String actSpdSp;
	String regdt;
}


