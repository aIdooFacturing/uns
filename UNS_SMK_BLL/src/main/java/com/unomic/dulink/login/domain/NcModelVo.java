package com.unomic.dulink.login.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NcModelVo {

	int id;
	String manufacture;
	String model;
	String type;
}
