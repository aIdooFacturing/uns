<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/custom_evt.js"></script>
<script src="${ctxPath }/js/rotate.js"></script>  
<script type="text/javascript">
var selected_bg = "rgb(192,193,89)"
var selected_txt_color = "white";

$(function () {
	setEl();
	bindEvt();
	//drawMachineLabel();
	timeOffset = $("#time").offset();
	addLabelPos();
	drawSunNMoon();
	setToday();
	getDvcList();
});

function getDvcList(){
	var url = "${ctxPath}/chart/getAllDvcList.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var option = "<option value='d0'>마지막 통신시간</option>";
			$(json).each(function(idx, data){
				option += "<option value=d" + data.dvcId + ">" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";
			});
			
			$("#dvcSelector").html(option).change(getLastConnection);
		}
	});
};

function getLastConnection(){
	var id = this.value.substr(1);
	var url = "${ctxPath}/chart/getLastConnection.do"
	var param = "dvcId=" + id;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var status = data.status + " - " + data.startDateTime
			if(data==null) status = "";
			$("#lastStatus").html(status).css("color", "white");	
		}, error : function(e1,e2,e3){
			$("#lastStatus").html("").css("color", "white");
		}
	});
};

function setToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	var minute = addZero(String(date.getMinutes())).substr(0,1);
	var today = year + "-" + month + "-" + day
	
	$("#sDate").val(today);
};

var deg =  30;
function rotateNav(time){
	if(time<=350/6){
		$("#circleNav div").each(function(idx, data){
			$(data).css({
				"-webkit-transform": "rotate(" + (angleArray[idx] + time) + "deg)",
			});
		});
		setTimeout(function(){
			rotateNav(time);
		}, 5);
	}else{
		for(var i = 0 ; i < angleArray.length; i++){
			angleArray[i] = angleArray[i] + 360/6;
		};
		
		$("#circleNav div").css("color", "gray");
		$("#circleNav div").each(function(idx, data){
			if($(data).css("transform")=="matrix(0.882948, 0.469472, -0.469472, 0.882948, 0, 0)"){
				$(data).css("color" , "white");
			};	
		});
	};
	time++;
};

function drawCloseMark(){
	$("#lLine, #fline").animate({
		"opacity" : 0
	});
	
	rotateBar(1, "close");
};

function rotateBar(deg, ty){
	$("#sLine").css({
		"-webkit-transform": "rotate(" + deg + "deg)"	
	});	
	
	$("#tLine").css({
		"-webkit-transform": "rotate(-" + deg + "deg)"	
	});	
	
	if(ty=="close"){
		if(deg<=40){
			deg++;
			setTimeout(function(){
				rotateBar(deg, ty);	
			}, 10);
		};	
	}else{
		if(deg>=0){
			deg--;
			setTimeout(function(){
				rotateBar(deg, ty);	
			}, 10);
		};
	}
	
};

function drawMenuMark(){
	$("#lLine, #fline").animate({
		"opacity" : 1
	});
	
	rotateBar(40, "open");
};

function drawSunNMoon(){
	var nightDiv = document.createElement("div");
	var nightHeight;
	$("#time div").each(function(idx, data){
		if($(data).html().substr(0,2)=="08"){
			nightHeight = $(data).offset().top + ($(data).height()/2)
		};
	});
	
	var moon = document.createElement("img");
	moon.setAttribute("src", ctxPath + "/images/moon.png");
	nightDiv.appendChild(moon);

	$(moon).css({
		"width" : "100%",
		"position" : "absolute", 
	});
	
	$(moon).css({
		"top" : (nightHeight/2) - ($(moon).height()/2)
	});
	
	$("body").prepend(nightDiv);
	
	$(nightDiv).css({
		"position" : "absolute",
		"z-index" : 2,
		"border-top" : getElSize(5) + "px solid white",
		"border-left" : getElSize(5) + "px solid white",
		"border-bottom" : getElSize(5) + "px solid white",
		"width" : getElSize(80),
		"left" : 0,
		"top" : $("#dataTable").offset().top,
		"height" : nightHeight
	});
	
	var dayDiv = document.createElement("div");
	var dayHeight;
	
	var sun = document.createElement("img");
	sun.setAttribute("src", ctxPath + "/images/sun.png");

	dayDiv.appendChild(sun);
	$(sun).css({
		"width" : "100%",
		"position" : "absolute",
	});
	
	$(sun).css({
		"top" : ((($("#time div").last().offset().top + $("#time div").height()) - nightHeight)/2) - ($(sun).height()/2),
	});
	
	$("body").prepend(dayDiv);
	
	$(dayDiv).css({
		"position" : "absolute",
		"z-index" : 2,
		"border-top" : getElSize(5) + "px solid white",
		"border-left" : getElSize(5) + "px solid white",
		"border-bottom" : getElSize(5) + "px solid white",
		"width" : getElSize(80),
		"left" : 0,
		"top" : nightHeight,
		"height" : ($("#time div").last().offset().top + $("#time div").height()) - nightHeight
	});
};

var labelPos = new Array();

function addLabelPos(){
	$("#time div").each(function(idx, data){
		labelPos.push($(data).offset().top);
	});
};

var timeOffset;
function bindEvt(){
	$("#time div").click(function(){
		$(".name").remove();	
		$("#time div").removeClass("selected");

		$(this).addClass("selected");
		
		$("#time div").not(".selected").css({
			"background-color" : "rgba(0,0,0,0)"	
		});
		
		var time = $(this).text().substr(0,2);
		selected_time = time;
		
	    window.scrollTo(0, 0);
	    
		if(!slideToggle){
			showEvt(time);
			slideToggle = !slideToggle;
		}else if(slideToggle && selected_div!=this){
			$("#dataTable").animate({
				"opacity" : 0
			}, function(){
				$("#dataTable").html("");	
			});
			
			$("#time").animate({
				"top" : 0
			});
			
			$(".tmp").animate({
				"height" : 0
			}, function(){
				$(".tmp").remove();
				showEvt(time);
			});
			
			$("#deviceDataTable").animate({
				"opacity" : 0
			});
		}else{
			$("#dataTable").animate({
				"opacity" : 0
			}, function(){
				$("#dataTable").html("");	
			});
			
			$("#time").animate({
				"top" : 0
			});
			
			$(".tmp").animate({
				"height" : 0
			}, function(){
				$(".tmp").remove();
			});
			
			
			//개별 장비이벤트 닫기
			$("#deviceDataTable").animate({
				"opacity" : 0
			});
			
			slideToggle = !slideToggle;
		}
		
		selected_div = this;
	});
    
	$("#time  div").bind("mouseover", function(evt){
		var selected = $(this).hasClass("selected");
		if(!selected){
			$(this).animate({
				"background-color" : selected_bg,
				"color" : selected_txt_color
			}, 200);		
		}
	});
	
	$("#time div").bind("mouseout", function(evt){
		var selected = $(this).hasClass("selected");
		
		if(!selected){
			$(this).animate({
				"background-color" : "rgba(0,0,0,0)",
				"color" : "white"
			}, 100);	
		};
	});
	
	$("#lineBox").click(function(){
		if(!nav_bar){
			$("#menu_box").animate({
				"width" : getElSize(700),
				"height" : getElSize(700)
			});
			
			$("#lineBox").css("right", 0);
			drawCloseMark();
		}else{
			$("#menu_box").animate({
				"width" : getElSize(150),
				"height" : getElSize(150)
			});
			drawMenuMark()
		};
		
		nav_bar = !nav_bar;
	});
	
	$('#circleNav').bind('mousewheel DOMMouseScroll', function(event){
			var scroll = window.scrollY;
		if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
			angle += (event.originalEvent.wheelDelta/10);
		}else{
			angle += (event.originalEvent.wheelDelta/10);
       	};
       	
       	$('#circleNav').css({
			"-webkit-transform": "rotate(" + angle + "deg)",
		});
       	
       	/* $("#circleNav div").css("color", "gray");
       	
		$("#circleNav div").each(function(idx, data){
			if($(data).offset().top >= ($('#circleNav').offset().top + ($('#circleNav').height()/2) - getElSize(50)) &&
				$(data).offset().top <= ($('#circleNav').offset().top + ($('#circleNav').height()/2) + getElSize(50))){
					$(data).css("color", "white");
			}
		}); */
		
       	document.getElementById('circleNav').scrollTop -= event. wheelDeltaY; 
        preventDefault(event);
   });
};

var angle = 0;
function preventDefault(e) {
	e = e || window.event;
	
	if (e.preventDefault) e.preventDefault();

	e.returnValue = false;  
};
	
var firstMove = true;
var firstY;

var nav_bar = false;
function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

var slideToggle = false;
var selected_time;
var selected_div;
function showEvt(time, ty){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	//var today = year + "-" + month + "-" + day; 
	today = $("#sDate").val();
	
	if(typeof(ty)=="undefined") ty = "";
	
	if(time>=20){
		//var today = new Date(today);
		var today = new Date($("#sDate").val());
		today.setDate(today.getDate()-1);
		var year = today.getFullYear();
		var month = addZero(String(today.getMonth()+1));
		var day = addZero(String(today.getDate()));
	
		var today = year + "-" + month + "-" + day;
	};
	
	
	var url = "${ctxPath}/chart/getAllEvt.do";
	var param = "sDate=" + today + " " + time + ":00" + 
				"&eDate=" + today + " " + time + ":59" + 
				"&type=" + ty + 
				"&dvcId=";
	
	console.log(param)
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$("#machine_selector").css("display", "inline");
			
			var json = data.dataList;
			var color = "";
			var tr;
			var ty;
			$(json).each(function(idx, data){
				
				//알람
				var alarm = "";
				var alarm1, alarm2, alarm3, alarm4, alarm5, alarm6, alarm7, alarm8, alarm9, alarm10;
				if(data.ncAlarmNum1==""){
					alarm1 = "";
				}else{
					alarm1 = data.ncAlarmNum1 + " - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum2==""){
					alarm2 = "";
				}else{
					alarm2 = data.ncAlarmNum2 + " - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum3==""){
					alarm3 = "";
				}else{
					alarm3 = data.ncAlarmNum3 + " - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum4==""){
					alarm4 = "";
				}else{
					alarm4 = data.ncAlarmNum4 + " - " +  decodeURIComponent(data.ncAlarmMsg4).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum5==""){
					alarm5 = "";
				}else{
					alarm5 = data.ncAlarmNum5 + " - " +  decodeURIComponent(data.ncAlarmMsg5).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum1==""){
					alarm6 = "";
				}else{
					alarm6 = data.pmcAlarmNum1 + " - " +  decodeURIComponent(data.pmcAlarmMsg1).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum2==""){
					alarm7 = "";
				}else{
					alarm7 = data.pmcAlarmNum2 + " - " +  decodeURIComponent(data.pmcAlarmMsg2).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum3==""){
					alarm8 = "";
				}else{
					alarm8 = data.pmcAlarmNum3 + " - " +  decodeURIComponent(data.pmcAlarmMsg3).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum4==""){
					alarm9 = "";
				}else{
					alarm9 = data.pmcAlarmNum4 + " - " +  decodeURIComponent(data.pmcAlarmMsg4).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum5==""){
					alarm10 = "";
				}else{
					alarm10 = data.pmcAlarmNum5 + " - " +  decodeURIComponent(data.pmcAlarmMsg5).replace(/\+/gi, " ") + "<br>";
				};
				
				
				alarm += alarm1 +
						alarm2 +
						alarm3 + 
						alarm4 + 
						alarm5 + 
						alarm6 +
						alarm7 + 
						alarm8 + 
						alarm9 +
						alarm10;
				
				if(data.status.toUpperCase()=="IN-CYCLE"){
					color = "green";
				}else if(data.status.toUpperCase()=="WAIT"){
					color = "yellow";
				}else if(data.status.toUpperCase()=="ALARM"){
					color = "red";
				}else if(data.status.toUpperCase()=="NO-CONNECTION"){
					color = "gray";
				};
				
				type = data.type;
				if(type=="rail"){
					type = "레일연삭";
				}else{
					type="블럭가공";
				};
				
				tr += "<Tr>" +
							"<td style='color:white; width:" + Math.round($(selected_div).width()) + "' align='center'>" + data.startDateTime.substr(11, 8) + "</td>" +
							"<td style='color:white' width='12%' align='center'>" + type +  "</td>" +
							"<td align='center' style='color:white; cursor : pointer'  width='12%' class='machine_name' id='m" + data.dvcId + "'> " + decodeURIComponent(data.name).replace(/\+/gi, " ").replace(/홈연삭/gi, "") + "</td>" +  
							"<Td align='center' style='color: " + color + "; font-weight:bolder' width='12%'>" + data.status + "</td>" +
							"<td style='color:white; padding-left:" + getElSize(15) + "' >" + alarm + "</td>" + 
					"</tr>";
			});
			
			$("#dataTable").html(tr).css({
				"margin-bottom" : getElSize(50)
			});
			
			$("#dataTable td").css({
				"font-size" : getElSize(40)
			});
			
			//machine name mouseover 
			$(".machine_name").bind("mouseover", function(evt){
				var selected = $(this).hasClass("selected_machine");
				if(!selected){
					$(this).animate({
						"background-color" : selected_bg,
						"color" : selected_txt_color
					}, 200);	
				}
			});
				
			$(".machine_name").bind("mouseout", function(evt){
				var selected = $(this).hasClass("selected_machine");
				if(!selected){
					$(this).animate({
						"background-color" : "rgba(0,0,0,0)",
						"color" : "white"
					}, 100);	
				}
			});
			
			$(".machine_name").click(function(){
				showAllDeviceEvt(this);
			});
			
			var height = $("#dataTable").height();
			var div = document.createElement("div");
			div.setAttribute("class", "tmp");
			
			$(selected_div).after(div);
			
			$("#dataTable").css({
				"left" : $(selected_div).offset().left - getElSize(5),
				"opacity" : 0,
				"top" : $(selected_div).height() + getElSize(10),
			});
			
			$(div).css({
				"border" : getElSize(5) + "px solid white"
			});
			
			$(div).animate({
				"height" : height,
			}, function(){
			});
			
			$("#dataTable").animate({
				"opacity" : 1
			});
	
			var index;
			$("#time div").each(function(idx, data){
				if($(data).html().substr(0,2)==selected_time){
					index = idx	
				};
			});
			$("#time").animate({
				"top" : -labelPos[index]
			});
		}
	});
};

var scroll;
function floatingName(el){
	$(".name").remove();
	scroll = window.scrollY;
	var top = $(el).offset().top - scroll;
	var left = $(el).offset().left;
	var width = $(el).width();
	var height = $(el).height();
	var name = $(el).html();
	
	var div = document.createElement("div");
	div.setAttribute("class", "name");
	
	var text = document.createTextNode(name);
	$(div).append(text);
	
	$("body").prepend(div);

	$(text).css({
		"vertical-align": "middle"
	});
	
	$(div).css({
		"position" : "fixed",
		"width" : width,
		"color" : "white",
		"height" : height,
		"top" : top,
		"left" : left,
		"background-color" :"rgb(192, 193, 89)",
		"z-index" : 9,
		"font-size" : getElSize(40),
		"text-align" : "center",
		"vertical-align": "middle"
	});
	
	$(div).animate({
		"top" : getElSize(0),
		"width" : getElSize(500),
		"height" : $("#time div").height(),
		"left" : $("#dataTable").offset().left + $("#dataTable").width(),
		"font-size" : getElSize(60),
	});
	
	scrollUp();
};

function scrollUp(){
	scroll-=10;
	scrollTo(0,scroll);
	
	if(scroll>=0) setTimeout(scrollUp);
};

function showAllDeviceEvt(el){
	$(".machine_name").removeClass("selected_machine");

	$(el).addClass("selected_machine");
	
	$(".machine_name").not(".selected_machine").css({
		"background-color" : "rgba(0,0,0,0)"	
	});
	
	floatingName(el);
	
	var url = ctxPath + "/chart/getAllEvt.do";
	var dvcId = el.id.substr(1);
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	//var today = year + "-" + month + "-" + day; 
	var today = $("#sDate").val();
	
	var sDate = new Date(today);
	sDate.setDate(sDate.getDate()-1);
	var year = sDate.getFullYear();
	var month = addZero(String(sDate.getMonth()+1));
	var day = addZero(String(sDate.getDate()));

	var sDate = year + "-" + month + "-" + day;
	
	//var sDate = $("#sDate").val();
	
	
	var param = "sDate=" + sDate + " 20:30" + 
				"&eDate=" + today + " 20:30" + 
				"&type=" + 
				"&dvcId=" + dvcId;
	
	$("#deviceDataTable").animate({
		"opacity" : 0
	});
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var tr = "";
			var color = "";
			$(json).each(function(idx, data){
				//알람
				var alarm = "";
				var alarm1, alarm2, alarm3, alarm4, alarm5, alarm6, alarm7, alarm8, alarm9, alarm10;
				if(data.ncAlarmNum1==""){
					alarm1 = "";
				}else{
					alarm1 = data.ncAlarmNum1 + " - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum2==""){
					alarm2 = "";
				}else{
					alarm2 = data.ncAlarmNum2 + " - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum3==""){
					alarm3 = "";
				}else{
					alarm3 = data.ncAlarmNum3 + " - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum4==""){
					alarm4 = "";
				}else{
					alarm4 = data.ncAlarmNum4 + " - " +  decodeURIComponent(data.ncAlarmMsg4).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum5==""){
					alarm5 = "";
				}else{
					alarm5 = data.ncAlarmNum5 + " - " +  decodeURIComponent(data.ncAlarmMsg5).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum1==""){
					alarm6 = "";
				}else{
					alarm6 = data.pmcAlarmNum1 + " - " +  decodeURIComponent(data.pmcAlarmMsg1).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum2==""){
					alarm7 = "";
				}else{
					alarm7 = data.pmcAlarmNum2 + " - " +  decodeURIComponent(data.pmcAlarmMsg2).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum3==""){
					alarm8 = "";
				}else{
					alarm8 = data.pmcAlarmNum3 + " - " +  decodeURIComponent(data.pmcAlarmMsg3).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum4==""){
					alarm9 = "";
				}else{
					alarm9 = data.pmcAlarmNum4 + " - " +  decodeURIComponent(data.pmcAlarmMsg4).replace(/\+/gi, " ") + "<br>";
				};
				if(data.pmcAlarmNum5==""){
					alarm10 = "";
				}else{
					alarm10 = data.pmcAlarmNum5 + " - " +  decodeURIComponent(data.pmcAlarmMsg5).replace(/\+/gi, " ") + "<br>";
				};
				
				
				alarm += alarm1 +
						alarm2 +
						alarm3 + 
						alarm4 + 
						alarm5 + 
						alarm6 +
						alarm7 + 
						alarm8 + 
						alarm9 +
						alarm10;
				
				if(data.status.toUpperCase()=="IN-CYCLE"){
					color = "green";
				}else if(data.status.toUpperCase()=="WAIT"){
					color = "yellow";
				}else if(data.status.toUpperCase()=="ALARM"){
					color = "red";
				}else if(data.status.toUpperCase()=="NO-CONNECTION"){
					color = "gray";
				};
				
				tr += "<tr>" + 
						"<Td align='center' width='10%' style='color:white'>" + data.startDateTime.substr(11, 8) + "</td>" +
						"<Td align='center' style='color: " + color + "; font-weight:bolder' width='20%'>" + data.status + "</td>" +
						"<td style='color:white; padding-left:" + getElSize(15) + "' style='color:white'>" + alarm + "</td>" +
					"</tr>";
			});
			
			$("#deviceDataTable").html(tr).css({
				"left" : $("#dataTable").offset().left + $("#dataTable").width(),
				"top" :  $("#dataTable").offset().top,
				"width" : getElSize(1500)
			});
			
			$("#deviceDataTable td").css({
				"font-size" : getElSize(40),
			});
			
			$("#deviceDataTable").animate({
				"opacity" : 1
			});
		}
	});
};

function drawMachineLabel(){
	var selector = "<span>전체</span>" +
	"<span>레일연삭</span>" +
	"<span>블럭가공</span>";
	
	$("#machine_selector").html(selector);
	$("#machine_selector span").css({
		"margin" : getElSize(20),
		"background-color" : "white",
		"color" : "black",
		"padding" : getElSize(20),
		"cursor" : "pointer"
	});
	
	
	$("#machine_selector span").click(function(){
		$("#machine_selector span").removeClass("selected_machine");
		
		$(this).addClass("selected_machine");
		$(this).css({
			"border" : getElSize(10) + "px solid white",
		});
		
		$("#machine_selector span").not(".selected_machine").css({
			"background-color" : "white",
			"color" : "black",
			"border" : "none"
		});
		
		var ty = "";
		if($(this).text()=="레일연삭"){
			ty = "rail";
		}else if($(this).text()=="블럭가공"){
			ty = "block";
		};
		
		showEvt(selected_time, ty);
	});
};

function setEl(){
	$("#grid").css({
		"width" : window.innerWidth,
		"height" : window.innerHeight,
		"overflow" : "hidden"
	});
	
	$("#time").css({
		"background-color" : "black",
		"font-size" : getElSize(70),
		"color" : "white",
		"width" : "6%",
		"left" : getElSize(60),
		//"height" : window.innerHeight*2,
		"position" : "absolute",
		"padding-left" : getElSize(20),
		
	});
	 
	$("#contents").css({
		"background-color" : "black",
		"color" : "white",
		"width" : "95%",
		"height" : window.innerHeight,
		"float" : "right",
		"overflow" : "auto"
	});
	
	$("#time div").css({
		"cursor" : "pointer" 
	});
	
	$("#dataTable").css({
		"position" : "absolute"
	});
	
	$(".minute").css({
		"font-size" : getElSize(30),
		"z-index " : -1
	});
	
	$("#deviceDataTable").css({
		"width" : getElSize(800),
		"position" : "absolute",
		"opacity" : 0
	});
	
	$("#menu_box").css({
		"width" : getElSize(150),
		"height" : getElSize(150),
		"position" : 'fixed',
		"border" : getElSize(5) + "px solid white",
		"top" : getElSize(50),
		"right" : getElSize(50),
		"background-color" : "black"
	});
	
	$(".menu").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"margin" : getElSize(50),
		"cursor" : "pointer"
	});
	
	$(".line").css({
		"width" : getElSize(90),
		"border" : getElSize(2) + "px solid white",
		"right" : getElSize(30),
	});
	
	$("#fLine").css({
		"position" : "absolute",
		"top" : getElSize(50)
	});
	
	$("#sLine, #tLine").css({
		"position" : "absolute",
		"top" : getElSize(70)
	});
	
	$("#lLine").css({
		"position" : "absolute",
		"top" : getElSize(90)
	});
	
	$("#lineBox").css({
		"position" : "absolute",
		"z-index" :9,
		"width" : $("#menu_box").width(),
		"top" : 0,
		"right" : 0,
		"height" : $("#menu_box").height(),
		"cursor" : "pointer",
	});
	
	$("#box").css({
		"position" : "absolute",
		"top" : (getElSize(700)/2) - ($("#box").height()/2) - getElSize(50),
		"left" : (getElSize(700)/2) - ($("#box").width()/2) - getElSize(50)
	});
	
	
	$("#sDate").css({
		"position" : "absolute",
		"font-size" : getElSize(40)
	});
	
	$("#sDate").css({
		"left" : getElSize(1000)
	});
	
	$("#dvcSelector").css({
		"position" : "absolute",
		"font-size" : getElSize(40),
		"left" : $("#sDate").offset().left + $("#sDate").width() + getElSize(50)
	});
	
	$("#lastStatus").css({
		"position" : "absolute",
		"font-size" : getElSize(40),
		"left" : $("#dvcSelector").offset().left + $("#dvcSelector").width() + getElSize(350)
	});
	
	/* $("#circleNav").css({ 
		"position" : "fixed",
		"color" : "white",
		"border" : "1px solid white",
		"width" : getElSize(800),
		"height" : getElSize(800),
		"border-radius" : "50%"
	}); 
	
	$("#circleWrapper").css({
		"z-index" : 9,
		"position" : "absolute",
		"bottom" : 0,
		"width" : getElSize(1200),
		"height" : getElSize(1200),
	});
	
	$("#circleNav").css({
		"right" : -$("#circleNav").width()/2,
		"bottom" : 0,
	});
	
	$("#circleWrapper").css({
		"left" : $("#circleNav").offset().left - getElSize(200),
		"top" : $("#circleNav").offset().top - getElSize(200)
	});
	
	$("#circleNav div").css({
		"position" : "absolute",
		"width" : getElSize(1300),
		"font-size" : getElSize(80),
		"font-weight" : "bolder",
	});

	$("#circleNav div span").css({
	});
	
	$("#circleNav div").each(function(idx, data){
		$(data).css({
			"right" : ($("#circleNav").width()/2) - ($(this).width()/2),
			"bottom" : ($("#circleNav").height()/2) - ($(this).height()/2),
			"-webkit-transform": "rotate(" + deg + "deg)",
		});
		angleArray.push(deg);
		deg -= 360/6;
	});
	
	$("#circleNav").css("color", "white"); */
	
	
	
	
	//$("#circleNav div:nth(0)").css("color", "white");
};

var angleArray = new Array();

</script>
<style type="text/css">
	body{
		background: black;
		overflow-x: hidden; 
	}
	
	*{
		margin: 0px;
		padding: 0px;
		font-family: monospace;
	}
	.selected, .selected_machine{
		background-color : #C47004;
		color : white;
	}
</style>
</head>
<body >
	<input id="sDate" type="date">
	<select id="dvcSelector"></select>
	<span id="lastStatus"></span>
	<div id="grid">
		<center>
			<div id="time">
				<div>20:00</div>
				<div>21:00</div>
				<div>22:00</div>
				<div>23:00</div>
				<div>24:00</div>
				<div>01:00</div>
				<div>02:00</div>
				<div>03:00</div>
				<div>04:00</div>
				<div>05:00</div>
				<div>06:00</div>
				<div>07:00</div>
				<div>08:00</div>
				<div>09:00</div>
				<div>10:00</div>
				<div>11:00</div>
				<div>12:00</div>
				<div>13:00</div>
				<div>14:00</div>
				<div>15:00</div>
				<div>16:00</div>
				<div>17:00</div>
				<div>18:00</div>
				<div>19:00</div>
			</div>
		</center>
		<table id="dataTable" style="width: 50%; ">
		</table>
		
		<table id="deviceDataTable">
		</table>
		
		<!-- <div id="menu_box">
			<div id="lineBox">
				<div id="fLine" class="line"></div>
				<div id="sLine" class="line"></div>
				<div id="tLine" class="line"></div>
				<div id="lLine" class="line"></div>
			</div>
			
			<div id="box">
				<div id="menu1" class="menu">MENU1</div>
				<div id="menu2" class="menu">MENU2</div>
				<div id="menu3" class="menu">MENU3</div>				
			</div>
		</div>
		
		<div id="circleNav">
			<div><span>menu1</span></div>
			<div><span>menu2</span></div>
			<div><span>menu3</span></div>
			<div><span>menu4</span></div>
			<div><span>menu5</span></div>
			<div><span>menu6</span></div>
		</div> -->
	</div>
</body>
</html>